"""This file contains all the classes you must complete for this project.

You can use the test cases in agent_test.py to help during development, and
augment the test suite with your own test cases to further test your code.

You must test your agent's strength against a set of agents with known
relative strength using tournament.py and include the results in your report.
"""
from random import randint


class Timeout(Exception):
    """Subclass base exception for code clarity."""
    pass


def custom_score(game, player):
    """Calculate the heuristic value of a game state from the point of view
    of the given player.

    Parameters
    ----------
    game : `isolation.Board`
        An instance of `isolation.Board` encoding the current state of the
        game (e.g., player locations and blocked cells).

    player : object
        A player instance in the current game (i.e., an object corresponding to
        one of the player objects `game.__player_1__` or `game.__player_2__`.)

    Returns
    ----------
    float
        The heuristic value of the current game state to the specified player.
    """

    return heuristic1(game, player)


def heuristic1(game, player):
    """Calculate the heuristic value of a game state from the point of view
    of the given player. This is the best performing heuristic among the ones tested.

    Parameters
    ----------
    game : `isolation.Board`
        An instance of `isolation.Board` encoding the current state of the
        game (e.g., player locations and blocked cells).

    player : object
        A player instance in the current game (i.e., an object corresponding to
        one of the player objects `game.__player_1__` or `game.__player_2__`.)

    Returns
    ----------
    float
        The heuristic value of the current game state to the specified player.
    """
    own_moves = len(game.get_legal_moves(player))
    opp_moves = len(game.get_legal_moves(game.get_opponent(player)))

    return float(own_moves - 2.5*opp_moves) / (game.width*game.height - len(game.get_blank_spaces()))


def heuristic2(game, player):
    """Calculate the heuristic value of a game state from the point of view
    of the given player. Second best heuristic among those tested.

    Parameters
    ----------
    game : `isolation.Board`
        An instance of `isolation.Board` encoding the current state of the
        game (e.g., player locations and blocked cells).

    player : object
        A player instance in the current game (i.e., an object corresponding to
        one of the player objects `game.__player_1__` or `game.__player_2__`.)

    Returns
    ----------
    float
        The heuristic value of the current game state to the specified player.
    """
    own_moves = len(game.get_legal_moves(player))
    opp_moves = len(game.get_legal_moves(game.get_opponent(player)))

    return float(own_moves - 3.0*opp_moves) / (game.width*game.height - len(game.get_blank_spaces()))


def heuristic3(game, player):
    """Calculate the heuristic value of a game state from the point of view
    of the given player. Third best heuristic among those tested.

    Parameters
    ----------
    game : `isolation.Board`
        An instance of `isolation.Board` encoding the current state of the
        game (e.g., player locations and blocked cells).

    player : object
        A player instance in the current game (i.e., an object corresponding to
        one of the player objects `game.__player_1__` or `game.__player_2__`.)

    Returns
    ----------
    float
        The heuristic value of the current game state to the specified player.
    """
    own_moves = len(game.get_legal_moves(player))
    opp_moves = len(game.get_legal_moves(game.get_opponent(player)))

    if len(game.get_blank_spaces()) < (game.width*game.height / 4.0):
        return float(own_moves - opp_moves) / (game.width*game.height - len(game.get_blank_spaces()))

    return float(own_moves - 2.5*opp_moves) / (game.width*game.height - len(game.get_blank_spaces()))


class CustomPlayer:
    """Game-playing agent that chooses a move using your evaluation function
    and a depth-limited minimax algorithm with alpha-beta pruning. You must
    finish and test this player to make sure it properly uses minimax and
    alpha-beta to return a good move before the search time limit expires.

    Parameters
    ----------
    search_depth : int (optional)
        A strictly positive integer (i.e., 1, 2, 3,...) for the number of
        layers in the game tree to explore for fixed-depth search. (i.e., a
        depth of one (1) would only explore the immediate sucessors of the
        current state.)

    score_fn : callable (optional)
        A function to use for heuristic evaluation of game states.

    iterative : boolean (optional)
        Flag indicating whether to perform fixed-depth search (False) or
        iterative deepening search (True).

    method : {'minimax', 'alphabeta'} (optional)
        The name of the search method to use in get_move().

    timeout : float (optional)
        Time remaining (in milliseconds) when search is aborted. Should be a
        positive value large enough to allow the function to return before the
        timer expires.
    """

    def __init__(self, search_depth=3, score_fn=custom_score,
                 iterative=True, method='minimax', timeout=10.):
        self.search_depth = search_depth
        self.iterative = iterative
        self.score = score_fn
        self.method = method
        self.time_left = None
        self.TIMER_THRESHOLD = timeout

    def get_move(self, game, legal_moves, time_left):
        """Search for the best move from the available legal moves and return a
        result before the time limit expires.

        This function must perform iterative deepening if self.iterative=True,
        and it must use the search method (minimax or alphabeta) corresponding
        to the self.method value.

        **********************************************************************
        NOTE: If time_left < 0 when this function returns, the agent will
              forfeit the game due to timeout. You must return _before_ the
              timer reaches 0.
        **********************************************************************

        Parameters
        ----------
        game : `isolation.Board`
            An instance of `isolation.Board` encoding the current state of the
            game (e.g., player locations and blocked cells).

        legal_moves : list<(int, int)>
            A list containing legal moves. Moves are encoded as tuples of pairs
            of ints defining the next (row, col) for the agent to occupy.

        time_left : callable
            A function that returns the number of milliseconds left in the
            current turn. Returning with any less than 0 ms remaining forfeits
            the game.

        Returns
        ----------
        (int, int)
            Board coordinates corresponding to a legal move; may return
            (-1, -1) if there are no available legal moves.
        """

        self.time_left = time_left

        # Perform any required initializations, including selecting an initial
        # move from the game board (i.e., an opening book), or returning
        # immediately if there are no legal moves

        if len(legal_moves) == 0:
            return (-1, -1)
        best_move = None

        try:
            # The search method call (alpha beta or minimax) should happen in
            # here in order to avoid timeout. The try/except block will
            # automatically catch the exception raised by the search method
            # when the timer gets close to expiring

            if self.iterative:
                depth = 1
                best_score = float("-inf")
                best_move = (-1, -1)
                while self.time_left() > self.TIMER_THRESHOLD:
                    if self.method == 'minimax':
                        score, move = self.minimax(game, depth)
                        if score > best_score:
                            best_score = score
                            best_move = move
                    else:
                        score, move = self.alphabeta(game, depth)
                        if score > best_score:
                            best_score = score
                            best_move = move
                    depth += 1

            else:
                # Run non ID versions
                if self.method == 'minimax':
                    _, best_move = self.minimax(game, self.search_depth)
                else:
                    _, best_move = self.alphabeta(game, self.search_depth)

        except Timeout:
            # Handle any actions required at timeout, if necessary
            pass

        # Return the best move from the last completed search iteration
        if best_move is not None:
            return best_move
        else:
            return legal_moves[randint(0, len(legal_moves) - 1)]

    def minimax(self, game, depth, maximizing_player=True):
        """Implement the minimax search algorithm as described in the lectures.

        Parameters
        ----------
        game : isolation.Board
            An instance of the Isolation game `Board` class representing the
            current game state

        depth : int
            Depth is an integer representing the maximum number of plies to
            search in the game tree before aborting

        maximizing_player : bool
            Flag indicating whether the current search depth corresponds to a
            maximizing layer (True) or a minimizing layer (False)

        Returns
        ----------
        float
            The score for the current search branch

        tuple(int, int)
            The best move for the current branch; (-1, -1) for no legal moves
        """
        if self.time_left() < self.TIMER_THRESHOLD:
            raise Timeout()

        def max_value(g, d, time_left=self.time_left):
            # Test for timeout approach in here as well
            if time_left() < self.TIMER_THRESHOLD:
                raise Timeout()

            # Get the correct player for the score function
            if maximizing_player:
                player = g.active_player
            else:
                player = g.inactive_player

            new_moves = g.get_legal_moves()
            if (len(new_moves) == 0) or (d == 0):
                return self.score(g, player)

            v = float("-inf")
            for m in new_moves:
                v = max(v, min_value(g.forecast_move(m), d - 1))

            return v

        def min_value(g, d, time_left=self.time_left):
            # Test for timeout approach in here as well
            if time_left() < self.TIMER_THRESHOLD:
                raise Timeout()

            # Get the correct player for the score function
            if maximizing_player:
                player = g.inactive_player
            else:
                player = g.active_player

            new_moves = g.get_legal_moves()
            if (len(new_moves) == 0) or (d == 0):
                return self.score(g, player)

            v = float("inf")
            for m in new_moves:
                v = min(v, max_value(g.forecast_move(m), d - 1))

            return v

        legal_moves = game.get_legal_moves()
        if maximizing_player:
            # Initialize variables
            v = float("-inf")
            best_move = (-1, -1)

            # Start with the maximizing layer for the max player
            for move in legal_moves:
                best_score = v
                v = max(v, min_value(game.forecast_move(move), depth - 1))
                if v > best_score:
                    best_move = move
        else:
            # Initialize variables
            v = float("inf")
            best_move = (-1, -1)

            # Start with the minimizing layer for the min player
            for move in legal_moves:
                best_score = v
                v = min(v, max_value(game.forecast_move(move), depth - 1))
                if v < best_score:
                    best_move = move

        return v, best_move

    def alphabeta(self, game, depth, alpha=float("-inf"), beta=float("inf"), maximizing_player=True):
        """Implement minimax search with alpha-beta pruning as described in the
        lectures.

        Parameters
        ----------
        game : isolation.Board
            An instance of the Isolation game `Board` class representing the
            current game state

        depth : int
            Depth is an integer representing the maximum number of plies to
            search in the game tree before aborting

        alpha : float
            Alpha limits the lower bound of search on minimizing layers

        beta : float
            Beta limits the upper bound of search on maximizing layers

        maximizing_player : bool
            Flag indicating whether the current search depth corresponds to a
            maximizing layer (True) or a minimizing layer (False)

        Returns
        ----------
        float
            The score for the current search branch

        tuple(int, int)
            The best move for the current branch; (-1, -1) for no legal moves
        """
        if self.time_left() < self.TIMER_THRESHOLD:
            raise Timeout()

        def max_value(g, d, a, b, time_left=self.time_left):
            # Test for timeout approach in here as well
            if time_left() < self.TIMER_THRESHOLD:
                raise Timeout()

            # Get the correct player for the score function
            if maximizing_player:
                player = g.active_player
            else:
                player = g.inactive_player

            new_moves = g.get_legal_moves()
            if (len(new_moves) == 0) or (d == 0):
                return self.score(g, player)

            v = float("-inf")
            for m in new_moves:
                v = max(v, min_value(g.forecast_move(m), d - 1, a, b))
                if v >= b:
                    return v
                a = max(a, v)

            return v

        def min_value(g, d, a, b, time_left=self.time_left):
            # Test for timeout approach in here as well
            if time_left() < self.TIMER_THRESHOLD:
                raise Timeout()

            # Get the correct player for the score function
            if maximizing_player:
                player = g.inactive_player
            else:
                player = g.active_player

            new_moves = g.get_legal_moves()
            if (len(new_moves) == 0) or (d == 0):
                return self.score(g, player)

            v = float("inf")
            for m in new_moves:
                v = min(v, max_value(g.forecast_move(m), d - 1, a, b))
                if v <= a:
                    return v
                b = min(b, v)

            return v

        legal_moves = game.get_legal_moves()
        if maximizing_player:
            # Initialize variables
            v = float("-inf")
            best_move = (-1, -1)

            # Start with the maximizing layer for the max player
            for move in legal_moves:
                best_score = v
                v = max(v, min_value(game.forecast_move(move), depth - 1, alpha, beta))
                if v > best_score:
                    best_move = move
                if v >= beta:
                    return v, best_move
                alpha = max(alpha, v)
        else:
            # Initialize variables
            v = float("inf")
            best_move = (-1, -1)

            # Start with the minimizing layer for the min player
            for move in legal_moves:
                best_score = v
                v = min(v, max_value(game.forecast_move(move), depth - 1, alpha, beta))
                if v < best_score:
                    best_move = move
                if v <= alpha:
                    return v, best_move
                beta = min(beta, v)

        return v, best_move

